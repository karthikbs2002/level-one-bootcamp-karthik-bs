//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>

float input();
float distance(float x1,float y1,float x2,float y2);
void output(float x1,float y1,float x2,float y2,float d);

int main()
    {
     float x1,y1,x2,y2,x;
     
     printf("enter x1 ");
     x1=input();
     
     printf("enter y1 ");
     y1=input();
     
     printf("enter x2 ");
     x2=input();
     
     printf("enter y2 ");
     y2=input();
     
     x=distance(x1,y1,x2,y2);
     output(x1,y1,x2,y2,x);
     return 0;

    }

float input()
    {
    float z;
    scanf("%f",&z);
    return z;
    }

float distance(float x1 ,float y1,float x2,float y2)
    {
        float c;
        c=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
        return c;
    }
    
void output(float x1,float y1,float x2,float y2,float d)
   {
    printf("distance between the cordinates (%.2f,%.2f )and (%.2f,%.2f)is %f",x1,y1,x2,y2,d);
   }