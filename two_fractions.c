//WAP to find the sum of two fractions.
#include<stdio.h>
#include<math.h>

struct fraction
{
    int n;
    int d;
};

typedef struct fraction Fraction;
Fraction input()
{
    Fraction f;
    printf("enter numerator of fractional number :");
    scanf("%d",&f.n);
    printf("enter denominator of fractional number :");
    scanf("%d",&f.d);
    printf("next\n");
    return f;
}


Fraction calculate(Fraction f1,Fraction f2)
{
    Fraction c;
    int a,b;
     c.n=(f1.n*f2.d)+(f2.n*f1.d);
     c.d=(f1.d*f2.d);
     
     return c;
}
     
int find_gcd(Fraction sum)
{
    Fraction c;
    int rem,gcd;
    while(c.d!=0)
    {
       rem=c.n%c.d;
       c.n=c.d;
       c.d=rem;
    }
    gcd=c.n;
    
    return gcd;
}


void output(Fraction f1,Fraction f2,Fraction sum,int gcd)
{
    printf("the sum of %d/ %d and %d/%d is %d/%d",f1.n,f1.d,f2.n,f2.d,sum.n/gcd,sum.d/gcd);
    
}

int main()
{
   
    Fraction f1,f2,sum;
    int gcd;
    
    f1=input();
    f2=input();

    sum=calculate(f1,f2);
    
    gcd=find_gcd(sum);
    
    output(f1,f2,sum,gcd);
    
    return 0;
}
