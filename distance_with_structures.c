//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>

struct point
{
    float x;
    float y;
};
typedef struct point Point;
Point input()
{
    Point p;
    printf("enter x cordinate :");
    scanf("%f",&p.x);
    printf("enter y cordinate :");
    scanf("%f",&p.y);
    return p;
}
float compute(Point p1,Point p2)
{
    float c;
    c=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
    return c;
}

void output(Point p1,Point p2,float d)
{
    printf("the distance between (%.2f,%.2f) and (%.2f,%.2f) is %f",p1.x,p1.y,p2.x,p2.y,d);
}

int main()
{
    float d;
    Point p1,p2;
    p1=input();
    p2=input();
    d=compute(p1,p2);
    output(p1,p2,d);
    return 0;
}